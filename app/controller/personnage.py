#encoding: utf-8

from datetime import datetime

from flask import g, render_template, redirect, url_for, flash, request
from flask_login import current_user

from app.controller.controller import Controller
from app.form.personnage import PersonnageCreationForm
from app.model.entite import Entite as EntiteModel
from app.model.personnage import Personnage as PersonnageModel

EMPLACEMENTS = {
    0   : "head",
    1   : "neck",
    2   : "chest",
    3   : "left_hand",
    4   : "right_hand",
    5   : "belt",
    6   : "left_finger",
    7   : "right_finger",
    8   : "legs",
    9   : "feet",
    10  : "bag",
}

class Personnage(Controller):
    def creation(self):
        if not current_user.is_authenticated:
            return redirect(url_for("core.inscription"))
        g.form = PersonnageCreationForm()
        if g.form.validate_on_submit():
            entite = EntiteModel.query.filter_by(nom=g.form.nom.data).first()
            if entite:
                flash("Ce nom de personnage existe déjà, merci d'en choisir un autre.", "error")
            elif len(g.form.nom.data)>40:
                flash("Ce nom de personnage est trop grand, merci d'en choisir un de moins de 40 caractères.", "error")
            else:
                # On crée d'abord une entité pour le personnage
                entite = EntiteModel()
                entite.nom = g.form.nom.data
                entite.sexe = g.form.sexe.data
                entite.type = 1
                entite.image = 1
                entite.zone_id = 1
                entite.pos_x = 3
                entite.pos_y = 3
                entite.dla = datetime.now()
                entite.save()
                # Puis on crée un personnage que l'on rattache à l'entité
                personnage = PersonnageModel()
                personnage.user_id = current_user.id
                personnage.entite_id = entite.id
                personnage.save()
                return redirect(url_for("zone.vue"))
        return render_template("personnage/creation.html")

    def tour(self):
        g.personnage = PersonnageModel.query.filter_by(user_id=current_user.id).first()
        if g.personnage is None:
            return redirect(url_for("personnage.creation"))
        g.personnage.entite.declenchement_dla()
        return render_template("personnage/tour.html")

    def profil(self):
        g.menu = "profil"
        g.personnage = PersonnageModel.query.filter_by(user_id=current_user.id).first()
        if g.personnage is None:
            return redirect(url_for("personnage.creation"))
        return render_template("personnage/profil.html")

    def inventaire(self):
        g.menu = "inventaire"
        g.personnage = PersonnageModel.query.filter_by(user_id=current_user.id).first()
        if g.personnage is None:
            return redirect(url_for("personnage.creation"))
        g.emplacements = EMPLACEMENTS
        # On génère l'équipement du personnage pour l'affichage
        g.equipement = []
        for key, emplacement in EMPLACEMENTS.items():
            g.equipement.append({
                "emplacement": emplacement,
            })
            for objet in g.personnage.entite.inventaire:
                if objet.place-900==key:
                    g.equipement[key]["nom"] = objet.objet.nom
                    g.equipement[key]["image"] = objet.objet.image
        # On génère l'inventaire du personnage pour l'affichage
        g.inventaire = []
        g.y_max = 1+int(g.personnage.entite.inventaire_taille_max/6)
        g.x_max = 4
        print(g.y_max)
        for key in range(g.personnage.entite.inventaire_taille_max):
            g.inventaire.append({
                "place": key,
            })
            for objet in g.personnage.entite.inventaire:
                if objet.place==key:
                    g.inventaire[key]["nom"] = objet.objet.nom
                    g.inventaire[key]["image"] = objet.objet.image
        print(g.inventaire)
        return render_template("personnage/inventaire.html")

    def action(self):
        g.personnage = PersonnageModel.query.filter_by(user_id=current_user.id).first()
        if g.personnage is None:
            return redirect(url_for("personnage.creation"))
        print("Action: %s" % request.form["action"])
        if request.form["action"]=="deplacement":
            g.personnage.entite.deplacer(int(request.form["x"]), int(request.form["y"]))
        elif request.form["action"]=="attaque":
            g.personnage.entite.attaquer(int(request.form["x"]), int(request.form["y"]))
        return redirect(url_for('zone.vue'))

