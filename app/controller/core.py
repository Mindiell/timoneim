#encoding: utf-8

from flask import g, render_template, redirect, url_for, flash, session
from flask_login import current_user, login_user, logout_user

from app.controller.controller import Controller
from app.form.user import UserSubscribeForm, UserLoginForm
from app.model.user import User as UserModel
from app.model.personnage import Personnage as PersonnageModel

class Core(Controller):
    def accueil(self):
        g.menu = "accueil"
        g.form = UserLoginForm()
        return render_template('core/accueil.html')

    def regles(self):
        g.menu = "regles"
        if current_user.is_authenticated:
            g.personnage = PersonnageModel.query.filter_by(user_id=current_user.id).first()
        return render_template('core/regles.html')

    def inscription(self):
        g.menu = "inscription"
        g.form = UserSubscribeForm()
        if g.form.validate_on_submit():
            # TODO; gérer l'égalité entre les mots de passe
            user = UserModel.query.filter_by(login=g.form.login.data).first()
            if user:
                flash("Ce nom d'utilisateur existe déjà, merci d'en choisir un autre.")
            else:
                user = UserModel()
                user.login = g.form.login.data
                user.password = g.form.password.data
                user.email = g.form.email.data
                # TODO; gérer l'activation par mail
                user.active = 1
                user.admin = 0
                user.save()
                # TODO: rediriger vers la page de création d'un personnage
                login_user(user)
                return redirect(url_for('personnage.creation'))
        return render_template('core/inscription.html')

    def connexion(self):
        g.form = UserLoginForm()
        if g.form.validate_on_submit():
            user = UserModel.query.filter_by(login=g.form.login.data, password=g.form.password.data).first()
            if user:
                if user.is_active:
                    login_user(user)
                    if len(user.personnage)==0:
                        # Personnage non encore créé
                        return redirect(url_for('personnage.creation'))
                    else:
                        # Affichage de la page de début de tour
                        return redirect(url_for('personnage.tour'))
        return redirect(url_for('core.accueil'))

    def deconnexion(self):
        logout_user()
        session.clear()
        return redirect(url_for('core.accueil'))

