#encoding: utf-8

from flask import g, request, render_template, redirect, url_for, flash, session, current_app
from flask_login import current_user

from app.controller.controller import Controller
from app.model.zone import Zone as ZoneModel
from app.model.entite import Entite as EntiteModel

class Zone(Controller):
    def vue(self):
        if not current_user.is_authenticated:
            return redirect(url_for('core.accueil'))
        g.menu = "vue"
        # On récupère le personnage actuel
        g.personnage = current_user.personnage[0]
        # On récupère la zone dans laquelle le personnage évolue
        g.zone = g.personnage.entite.zone
        # On récupère le fond de carte vue par le personnage
        g.vue = g.zone.vue(
            g.personnage.entite.pos_x,
            g.personnage.entite.pos_y
        )
        # On récupère les entités visibles par le personnage
        g.entites = EntiteModel.query.filter(
            EntiteModel.zone_id==g.personnage.entite.zone_id,
            EntiteModel.pos_x>=g.personnage.entite.pos_x-current_app.config['DISTANCE_VUE'],
            EntiteModel.pos_x<=g.personnage.entite.pos_x+current_app.config['DISTANCE_VUE'],
            EntiteModel.pos_y>=g.personnage.entite.pos_y-current_app.config['DISTANCE_VUE'],
            EntiteModel.pos_y<=g.personnage.entite.pos_y+current_app.config['DISTANCE_VUE'],
        ).all()
        # On liste les actions possibles pour le personnage, par défaut, aucune
        g.actions = []
        # Cible par défaut, le joueur lui-même
        g.cible = {
            'nom': g.personnage.entite.nom,
            'x': g.personnage.entite.pos_x,
            'y': g.personnage.entite.pos_y,
            'entite': None,
        }
        # Prise en compte de la case ciblée par le joueur
        if request.method=="POST":
            g.cible['x'] = request.form['x']
            g.cible['y'] = request.form['y']
            # On cherche une entité à cette position, dans cette zone
            entite = EntiteModel.query.filter_by(
                zone_id=g.personnage.entite.zone_id,
                pos_x=g.cible['x'],
                pos_y=g.cible['y']
            ).first()
            if entite is not None:
                g.cible['entite'] = entite
                g.cible['nom'] = entite.nom
                # Quel type d'entité ? Et est-ce le personnage lui-même ?
                if g.cible['x']==g.personnage.entite.pos_x and g.cible['y']==g.personnage.entite.pos_y:
                    # On pointe le personnage
                    pass
                else:
                    # Il s'agit d'une autre entite, on peut l'attaquer
                    # Pour le moment, uniquement les monstres et les autres joueurs
                    if entite.type==1 or entite.type==2:
                        g.actions.append("attaque")
            else:
                # Aucune entité
                g.cible['nom'] = "Sol"
                # On peut donc se déplacer ici
                g.actions.append("deplacement")
        return render_template('zone/vue.html')

    def generate(self):
        zone = ZoneModel.query.filter_by(id=1).first()
        zone.generate_monster()
        return "OK"

