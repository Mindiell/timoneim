#encoding: utf-8

from app.controller.admin import Admin
from app.controller.core import Core
from app.controller.personnage import Personnage
from app.controller.zone import Zone

routes = [
    ('/', Core.as_view('accueil')),
    ('/regles', Core.as_view('regles')),
    ('/inscription', Core.as_view('inscription'), ['GET', 'POST']),
    ('/connexion', Core.as_view('connexion'), ['POST']),
    ('/deconnexion', Core.as_view('deconnexion')),

    ('/personnage/creation', Personnage.as_view('creation'), ['GET', 'POST']),
    ('/personnage/tour', Personnage.as_view('tour')),
    ('/personnage/profil', Personnage.as_view('profil')),
    ('/personnage/inventaire', Personnage.as_view('inventaire')),
    ('/personnage/action', Personnage.as_view('action'), ['POST']),

    ('/zone/vue', Zone.as_view('vue'), ['GET', 'POST']),
    ('/zone/generate', Zone.as_view('generate')),

    ('/admin/login', Admin.as_view('login'), ['GET', 'POST']),
]

apis = [
]
