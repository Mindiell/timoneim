#encoding: utf-8

from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, HiddenField, IntegerField
from wtforms.validators import DataRequired, NumberRange

class PersonnageCreationForm(FlaskForm):
    nom = StringField('Nom', validators=[DataRequired()])
    sexe = SelectField('Sexe', choices=(
        ('1', 'Mâle'),
        ('2', 'Femelle'),
    ), validators=[DataRequired()])

