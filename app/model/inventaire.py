#encoding: utf-8

from app import admin, db
from app.model.model import Model, View

class Inventaire(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    # Un inventaire est lié à une entité
    entite_id = db.Column(db.Integer, db.ForeignKey('entite.id'))
    entite = db.relationship('Entite', backref=db.backref('inventaire'))
    # Un inventaire est contient des objets
    objet_id = db.Column(db.Integer, db.ForeignKey('objet.id'))
    objet = db.relationship('Objet')
    # Chaque objet est placé quelque part dans le sac de l'entité
    place = db.Column(db.Integer)

# Model will be automatically managed through flask-admin module
admin.add_view(View(Inventaire, db.session))
