#encoding: utf-8

from random import choice

from flask import current_app

from app import admin, db
from app.model.model import Model, View

GROUNDS = {
    " ": "0",
    ".": "91",
    "#": "44",
}

class Zone(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(200))
    # Fichier de la carte du jeu
    carte = db.Column(db.String(200))
    max_monstres = db.Column(db.Integer)

    def __repr__(self):
        return '%s (%s)' % (self.nom, self.id)

    def vue(self, center_x, center_y):
        """
        Retourne les cases à afficher dans la vue centrée en (x,y).
        On utilise une largeur de vue de 5 pour débuter
        """
        distance_vue = current_app.config['DISTANCE_VUE']
        with open('maps/%s' % self.carte) as f:
            lines = f.read().splitlines()
        height = len(lines)
        view = []
        for y in range(center_y-distance_vue, center_y+distance_vue):
            # On est hors carte, on renvoit une ligne vide
            if y<0 or y>=height:
                cells = [{
                        "ground": GROUNDS[" "],
                        "x": x,
                        "y": y,
                        "entity": None,
                    } for x in range(-distance_vue, distance_vue)
                ]
            else:
                cells = []
                for x in range(center_x-distance_vue, center_x+distance_vue):
                    # On est hors carte, on renvoit une case vide
                    if x<0 or x>=len(lines[y]):
                        cells.append({
                            "ground": GROUNDS[" "],
                            "x": x,
                            "y": y,
                            "entity": None,
                        })
                    else:
                        cells.append({
                            "ground": GROUNDS[lines[y][x]],
                            "x": x,
                            "y": y,
                            "entity": None,
                        })
            view.append(cells)
        # On a obtenu la vue désirée, on retourne celle-ci
        return view

    def generate_monster(self):
        """
        Cette fonction crée un nouveau monstre dans la zone, si nécessaire.
        """
        # On compte les entités de type monstre présents sur la zone
        if self.entites.filter_by(type=2).count()<self.max_monstres:
            # On liste les monstres 
            monstres = self.monstres.all()
            #print(", ".join(["%s (%s/%s)" % (monstre.monstre_gabarit.nom, monstre.max_monstres, monstre.taux) for monstre in monstres]))
            # TODO: il faut choisir un monstre parmi la liste donnée, suivant le taux du monstre
            # TODO: on ne peut choisir qu'entre les monstres qui n'ont pas atteint leur max
            gabarit = monstres[0].monstre_gabarit
            # Génération du monstre sélectionné dans la zone
            return gabarit.generate_monster(self)

    def get_case_vide(self):
        """
        Cette fonction permet de retourner une case vide de la zone afin d'y placer un élément
        """
        from app.model.entite import Entite as EntiteModel
        with open('maps/%s' % self.carte) as f:
            lines = f.read().splitlines()
        # On parcourt chaque ligne afin de récolter les coordonnées disponibles
        cases = []
        for y in range(len(lines)):
            for x in range(len(lines[y])):
                if lines[y][x]!=' ':
                    # On vérifie maintenant qu'aucune entite n'est présente
                    if EntiteModel.query.filter_by(zone_id=self.id,pos_x=x,pos_y=y).first() is None:
                        cases.append((x,y))
        #On choisit une case au hasard parmi les cases trouvées
        return choice(cases)

# Model will be automatically managed through flask-admin module
admin.add_view(View(Zone, db.session))
