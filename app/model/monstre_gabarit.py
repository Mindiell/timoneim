#encoding: utf-8

from datetime import datetime

from app import admin, db
from app.model.model import Model, View

class MonstreGabarit(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)

    # Caractéristiques spécifiques du monstre utilisées pour générer son entité lors de sa création
    nom = db.Column(db.String(200))
    image = db.Column(db.Integer)

    vigueur = db.Column(db.Integer)
    resistance = db.Column(db.Integer)
    agilite = db.Column(db.Integer)
    constitution = db.Column(db.Integer)
    esprit = db.Column(db.Integer)
    sagesse = db.Column(db.Integer)
    intellect = db.Column(db.Integer)
    essence = db.Column(db.Integer)
    niveau = db.Column(db.Integer)
    pv = db.Column(db.Integer)
    pe = db.Column(db.Integer)
    pm = db.Column(db.Integer)
    pa = db.Column(db.Integer)
    duree_dla = db.Column(db.Integer)

    def __repr__(self):
        return 'Gabarit %s (%s)' % (self.nom, self.id)

    def generate_monster(self, zone):
        """
        Cette fonction génère un monstre dans la zone spécifiée.
        """
        from app.model.entite import Entite as EntiteModel
        from app.model.monstre import Monstre as MonstreModel
        entite = EntiteModel(type=2, zone_id=zone.id)
        entite.nom = self.nom
        entite.image = self.image
        entite.vigueur = self.vigueur
        entite.resistance = self.resistance
        entite.agilite = self.agilite
        entite.constitution = self.constitution
        entite.esprit = self.esprit
        entite.sagesse = self.sagesse
        entite.intellect = self.intellect
        entite.essence = self.essence
        entite.pv = self.pv
        entite.pe = self.pe
        entite.pm = self.pm
        entite.pa = self.pa
        entite.dla = datetime.now()
        case = zone.get_case_vide()
        entite.pos_x = case[0]
        entite.pos_y = case[1]
        entite.save()
        monstre = MonstreModel(entite_id=entite.id, gabarit_id=self.id)
        monstre.save()
        return self.nom

# Model will be automatically managed through flask-admin module
admin.add_view(View(MonstreGabarit, db.session))

