#encoding: utf-8

from datetime import datetime, timedelta
from random import randint

from flask import current_app, flash, g

from app import admin, db
from app.model.model import Model, View

# Une entité est un truc vivant dans le jeu

class Entite(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    # Type d'entité :
    #   1. Personnage
    #   2. Monstre
    type = db.Column(db.Integer)
    # Caractéristiques de l'entité
    nom = db.Column(db.String(200))
    image = db.Column(db.Integer)
    vigueur = db.Column(db.Integer, default=10)
    resistance = db.Column(db.Integer, default=10)
    agilite = db.Column(db.Integer, default=10)
    constitution = db.Column(db.Integer, default=10)
    esprit = db.Column(db.Integer, default=10)
    sagesse = db.Column(db.Integer, default=10)
    intellect = db.Column(db.Integer, default=10)
    essence = db.Column(db.Integer, default=10)
    pv = db.Column(db.Integer, default=10)
    pe = db.Column(db.Integer, default=10)
    pm = db.Column(db.Integer, default=10)
    pa = db.Column(db.Integer, default=12)
    dla = db.Column(db.DateTime)
    # Position de l'entité
    zone_id = db.Column(db.Integer, db.ForeignKey('zone.id'))
    zone = db.relationship('Zone', backref=db.backref('entites', lazy='dynamic'))
    pos_x = db.Column(db.Integer)
    pos_y = db.Column(db.Integer)
    # Inventaire de l'entité
    inventaire_taille_max = db.Column(db.Integer, default=1)

    @property
    def pv_max(self):
        return int((self.constitution + self.essence)/2)

    @property
    def pe_max(self):
        return self.resistance

    @property
    def pm_max(self):
        return self.sagesse

    @property
    def attaque(self):
        return self.agilite

    @property
    def parade(self):
        return self.constitution

    @property
    def bonus_degats(self):
        return int(self.vigueur/5)

    @property
    def distance_attaque(self):
        return int(self.agilite/5)

    @property
    def attaque_magique(self):
        return self.intellect

    @property
    def parade_magique(self):
        return self.essence

    @property
    def bonus_degats_magique(self):
        return int(self.esprit/5)

    @property
    def distance_attaque_magique(self):
        return int(self.intellect/5)

    @property
    def poids_portable(self):
        return self.vigueur

    def __repr__(self):
        return '%s (%s)' % (self.nom, self.id)

    def declenchement_dla(self):
        change = False
        while self.dla<datetime.now():
            # TODO: ceci ne fonctionne que pour les joueurs !!!
            self.dla += timedelta(hours=current_app.config['DUREE_DLA'])
            change = True
        if change:
            g.dla = True
            # On réinitialise les PA
            self.pa = current_app.config['PA_MAX']
            # On régénère les PV, PE, PM
            if self.pv<self.pv_max:
                g.dla_pv = 2
                self.pv = min(self.pv+2, self.pv_max)
            if self.pe<self.pe_max:
                g.dla_pe = 2
                self.pe = min(self.pe+2, self.pe_max)
            if self.pm<self.pm_max:
                g.dla_pm = 2
                self.pm = min(self.pm+2, self.pm_max)
            self.save()

    def deplacer(self, cible_x, cible_y):
        # On vérifie que la case est atteignable
        if abs(self.pos_x-cible_x)>1 or abs(self.pos_y-cible_y)>1:
            flash("La case est trop éloignée.")
            return
        # On vérifie qu'il n'existe pas déjà une entité à cet endroit
        entite = Entite.query.filter_by(
            zone_id=self.zone_id,
            pos_x=cible_x,
            pos_y=cible_y
        ).first()
        if entite is not None:
            flash("La case est déjà occupée.")
            return
        # On vérifie qu'il reste assez de PA à l'entité
        if self.pa<2:
            flash("Il ne vous reste pas assez de PA.")
            return
        # On déplace l'entité et on retire les PA
        self.pos_x = cible_x
        self.pos_y = cible_y
        self.pa = self.pa - 2
        self.save()

    def attaquer(self, cible_x, cible_y):
        # On vérifie que la case est ciblable
        if abs(self.pos_x-cible_x)>1 or abs(self.pos_y-cible_y)>1:
            flash("La case est trop éloignée.")
            return
        # On vérifie qu'il y a bien une entité à cet endroit
        entite = Entite.query.filter_by(
            zone_id=self.zone_id,
            pos_x=cible_x,
            pos_y=cible_y
        ).first()
        if entite is None:
            flash("La case est vide.")
            return
        # On vérifie qu'il reste assez de PA à l'entité
        if self.pa<4:
            flash("Il ne vous reste pas assez de PA.")
            return
        # On vérifie qu'il reste assez de PE à l'entité
        if self.pe<1:
            flash("Il ne vous reste pas assez de PE.")
            return
        # On gère l'attaque
        print("Combat entre %s et %s" % (self.nom, entite.nom))
        print("   Attaque : % 2d" % self.attaque)
        print("   Parade  : % 2d" % entite.parade)
        chances = self.attaque / (self.attaque + entite.parade) * 100
        print(" => Chances: %3d %%" % chances)
        de = randint(1, 100)
        print(" Jet de dé : %3d %%" % de)
        # On doit faire moins qque les chances de toucher, ou une réussite automatique
        if de<=chances or de<=5:
            print(" => Réussite !")
            # TODO: Gérer les dégâts donnés via l'arme équipée
            # En cas de réussite critique, les dégâts sont maximum
            if de<=5:
                degats = 3 + self.bonus_degats
            else:
                degats = randint(1,3) + self.bonus_degats
            print(" => Dégâts : % 2d" % degats)
            # On mets à jour les PV du défenseur, en les réduisant de son armure
            # TODO: Prendre en compte l'armure du défenseur via son équipement
            print("   Armure : % 2d" % 0)
            entite.pv = entite.pv - degats
            print(" => PVs finaux : % 2d" % entite.pv)
            # Si le défenseur est mort, on gère ça
            if entite.pv<=0:
                # On fait gagner de l'expérience au joueur qui tue un adversaire
                if self.type==1:
                    if entite.type==1:
                        entite_niveau = entite.personnage[0].niveau
                    elif entite.type==2:
                        entite_niveau = entite.monstre[0].gabarit.niveau
                    print("%s / %s" % (self.personnage[0].niveau, entite_niveau))
                print("L'adversaire est mort...")
                flash("%s est mort !" % entite.nom, "success")
                # TODO: Supprimer l'entité et son personnage/monstre lié
                if entite.type==2:
                    entite.monstre[0].delete()
                    entite.delete()
            else:
                flash("Vous touchez %s !" % entite.nom, "success")
        else:
            print(" => Raté !")
            flash("Vous ratez %s..." % entite.nom, "error")
        # On retire les PA et les PE
        self.pa = self.pa - 4
        self.pe = self.pe - 1
        self.save()

class EntiteAdmin(View):
    column_exclude_list = [
        'vigueur',
        'resistance',
        'agilite',
        'constitution',
        'esprit',
        'sagesse',
        'intellect',
        'essence',
        'pv',
        'pe',
        'pm',
    ]

# Model will be automatically managed through flask-admin module
admin.add_view(EntiteAdmin(Entite, db.session))

