#encoding: utf-8

from app import admin, db
from app.model.model import Model, View

class Monstre(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    entite_id = db.Column(db.Integer, db.ForeignKey('entite.id'))
    entite = db.relationship('Entite', backref=db.backref('monstre'))
    gabarit_id = db.Column(db.Integer, db.ForeignKey('monstre_gabarit.id'))
    gabarit = db.relationship('MonstreGabarit')

    # Caractéristiques spécifiques au monstre

    def __repr__(self):
        return 'Monstre %s (%s)' % (self.entite.nom, self.id)

# Model will be automatically managed through flask-admin module
admin.add_view(View(Monstre, db.session))

