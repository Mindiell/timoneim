#encoding: utf-8

from app import admin, db
from app.model.model import Model, View

class Objet(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)

    # Caractéristiques spécifiques de l'objet
    nom = db.Column(db.String(200))
    image = db.Column(db.String(50))

    def __repr__(self):
        return '%s (%s)' % (self.nom, self.id)

# Model will be automatically managed through flask-admin module
admin.add_view(View(Objet, db.session))
