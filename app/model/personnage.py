#encoding: utf-8

from app import admin, db
from app.model.model import Model, View

class Personnage(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('personnage'))
    entite_id = db.Column(db.Integer, db.ForeignKey('entite.id'))
    entite = db.relationship('Entite', backref=db.backref('personnage'))

    # Caractéristiques spécifiques au personnage
    sexe = db.Column(db.Integer)
    portrait = db.Column(db.String(200), default='0.png')
    experience = db.Column(db.Integer, default=0)
    fortune = db.Column(db.Integer, default=0)
    banque = db.Column(db.Integer, default=0)
    poids_porte = db.Column(db.Integer, default=0)

    @property
    def jauge_pv(self):
        return (self.entite.pv / self.entite.pv_max) * 100

    @property
    def jauge_pe(self):
        return (self.entite.pe / self.entite.pe_max) * 100

    @property
    def jauge_pm(self):
        return (self.entite.pm / self.entite.pm_max) * 100

    @property
    def niveau(self):
        return self.experience

    def __repr__(self):
        return '%s (%s)' % (self.entite.nom, self.id)

class PersonnageAdmin(View):
    column_exclude_list = [
        'sexe',
        'portrait',
    ]

# Model will be automatically managed through flask-admin module
admin.add_view(PersonnageAdmin(Personnage, db.session))

