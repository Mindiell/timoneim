#encoding: utf-8

from app import admin, db
from app.model.model import Model, View

class MonstreZone(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    monstre_gabarit_id = db.Column(db.Integer, db.ForeignKey('monstre_gabarit.id'), nullable=False)
    monstre_gabarit = db.relationship('MonstreGabarit', backref=db.backref('zones', lazy='dynamic'))
    zone_id = db.Column(db.Integer, db.ForeignKey('zone.id'), nullable=False)
    zone = db.relationship('Zone', backref=db.backref('monstres', lazy='dynamic'))
    max_monstres = db.Column(db.Integer)
    taux = db.Column(db.Integer)

# Model will be automatically managed through flask-admin module
admin.add_view(View(MonstreZone, db.session))
