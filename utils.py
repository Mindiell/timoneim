#encoding: utf-8

from datetime import datetime

def utility_processor():
    def now():
        return datetime.now()

    return {
        'now': now,
    }
