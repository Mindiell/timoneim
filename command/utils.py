#encoding: utf-8

import click
from datetime import datetime
from flask.cli import with_appcontext

from app.model.zone import Zone

@click.group()
@click.pass_context
def utils(context):
    "Ensemble de commandes utilitaires pour le jeu"
    context.ensure_object(dict)
    #context.obj["offline"] = offline

@utils.command(help="Génère de nouveaux monstres sur les zones")
@click.pass_context
@with_appcontext
def generate(context):
    click.echo("Génération de monstres")
    zones = Zone.query.all()
    for zone in zones:
        click.echo("  Zone '%s'" % zone.nom)
        for i in range(10):
            monstre = zone.generate_monster()
            if monstre is not None:
                click.echo("    Génération d'un %s" % monstre)

