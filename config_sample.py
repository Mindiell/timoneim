#encoding: utf-8

import os

VERSION = ''

DEBUG = False
HOST = "0.0.0.0"
PORT = 5000
SECRET_KEY = "No secret key"

JINJA_ENV = {
    "TRIM_BLOCKS"   : True,
    "LSTRIP_BLOCKS" : True,
}

# defining base directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# defining database URI
SQLALCHEMY_DATABASE_URI = "mysql://username:password@server/db"
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Données spécifiques du jeu
DUREE_DLA = 12
PA_MAX = 12
DISTANCE_VUE = 7

