#!/bin/bash

export ENVIRONMENT_CONFIG=./tests/config_test_gitlab.py
export FLASK_APP='server.py'

flask db upgrade
nosetests --with-coverage --cover-html --cover-package=app/ --cover-erase --cover-html-dir=./tests/cover --nocapture
