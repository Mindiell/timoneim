-----------------------------------------------------------------------------
-- FICHIER DE POPULATION DE LA BASE DE DONNEES POUR UN DEBUT DE PARTIE.
--
-- !!!! TOUTES LES DONNEES SERONT SUPPRIMEES !!!!
-----------------------------------------------------------------------------

-- On nettoie toutes les tables
DELETE FROM entite;
DELETE FROM monstre;
DELETE FROM monstre_gabarit;
DELETE FROM monstre_zone;
DELETE FROM personnage;
DELETE FROM user;
DELETE FROM zone;

-- Administrateur
INSERT INTO user VALUES (1, 'admin', 'admin', '', 1, 1);

-- Gabarits de monstres
INSERT INTO monstre_gabarit VALUES (1, 'Lapin', 2, 3, 2, 5, 1, 1, 1, 1, 1, 3, 10, 0, 12, 12);

-- Zones
-- Ile de la bêta
INSERT INTO zone VALUES (1, 'Ile de la bêta', 'ile_beta', 5);
-- Et ses monstres
INSERT INTO monstre_zone VALUES (1, 1, 1, 10, 100);

