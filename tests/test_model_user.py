#encoding: utf-8

from setup import Setup

from app.model.user import User as UserModel

class TestModelUser(Setup):
    def test_creation(self):
        users = UserModel.query.all()
        nb_users = len(users)
        user = UserModel(login='test', password='test', email='', active=True, admin=True)
        user.save()
        users = UserModel.query.all()
        self.assertEqual(len(users), nb_users+1)

    def test_anonymous_user(self):
        user = UserModel()
        self.assertEqual(user.is_anonymous, True)
        user = UserModel(login='test', password='test', email='', active=True, admin=True)
        user.save()
        self.assertEqual(user.is_anonymous, False)
