#encoding: utf-8

from setup import Setup

class TestControllerPersonnage(Setup):
    def test_acces_creation_hors_connexion(self):
        result = self.client.get('/personnage/creation', follow_redirects=True)
        self.assertIn('Inscription', result.data.decode("utf-8"))

    def test_creation_ok(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo2",
                "password": "foo2",
            },
            follow_redirects=True
        )
        result = self.client.post(
            '/personnage/creation',
            data = {
                "nom": "Héros foo2",
                "sexe": "1",
            },
            follow_redirects=True
        )
        self.assertIn('Vue', result.data.decode("utf-8"))

    def test_creation_personnage_deja_existant(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo2",
                "password": "foo2",
            },
            follow_redirects=True
        )
        result = self.client.post(
            '/personnage/creation',
            data = {
                "nom": "Héros foo",
                "sexe": "1",
            },
            follow_redirects=True
        )
        self.assertIn('Ce nom de personnage existe déjà', result.data.decode("utf-8"))

    def test_affichage_profile(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo",
                "password": "foo",
            },
            follow_redirects=True
        )
        result = self.client.get('/personnage/profil')
        self.assertIn('Profil', result.data.decode("utf-8"))

    def test_affichage_profil_sans_personnage(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo2",
                "password": "foo2",
            },
            follow_redirects=True
        )
        result = self.client.get('/personnage/profil', follow_redirects=True)
        self.assertIn("Création de votre personnage", result.data.decode("utf-8"))

    def test_action_sans_personnage(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo2",
                "password": "foo2",
            },
            follow_redirects=True
        )
        result = self.client.post(
            '/personnage/action',
            data = {
                "action": "deplacement",
                "x": "1",
                "y": "1",
            },
            follow_redirects=True
        )
        self.assertIn("Création de votre personnage", result.data.decode("utf-8"))

    def test_action_deplacement(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo",
                "password": "foo",
            },
            follow_redirects=True
        )
        from app.model.entite import Entite 
        entite = Entite.query.filter_by(id=1).first()
        old_pa = entite.pa
        result = self.client.post(
            '/personnage/action',
            data = {
                "action": "deplacement",
                "x": "1",
                "y": "2",
            },
            follow_redirects=True
        )
        entite = Entite.query.filter_by(id=1).first()
        self.assertEqual(1, entite.pos_x)
        self.assertEqual(2, entite.pos_y)
        self.assertEqual(old_pa-2, entite.pa)
        self.assertIn("Vue", result.data.decode("utf-8"))

    def test_action_attaque(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo",
                "password": "foo",
            },
            follow_redirects=True
        )
        from app.model.entite import Entite 
        entite = Entite.query.filter_by(id=1).first()
        old_pa = entite.pa
        result = self.client.post(
            '/personnage/action',
            data = {
                "action": "attaque",
                "x": "2",
                "y": "1",
            },
            follow_redirects=True
        )
        entite = Entite.query.filter_by(zone_id=1, pos_x=2, pos_y=1).first()
        self.assertIsNone(entite)
        entite = Entite.query.filter_by(id=1).first()
        self.assertEqual(old_pa-4, entite.pa)
        self.assertIn("Vue", result.data.decode("utf-8"))

