#encoding: utf-8

import os

VERSION = 'TEST'

DEBUG = False
HOST = "0.0.0.0"
PORT = 5555
SECRET_KEY = "No secret key"

JINJA_ENV = {
    "TRIM_BLOCKS"   : True,
    "LSTRIP_BLOCKS" : True,
}

# defining base directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# defining database URI
SQLALCHEMY_DATABASE_URI = "mysql://root:root@mariadb/timoneim_test"
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Disable CSRF for tests
WTF_CSRF_ENABLED = False

# Données spécifiques du jeu
DUREE_DLA = 12
PA_MAX = 12
DISTANCE_VUE = 7

