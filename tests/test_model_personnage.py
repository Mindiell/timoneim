#encoding: utf-8

from setup import Setup

from app.model.entite import Entite
from app.model.personnage import Personnage

class TestModelPersonnage(Setup):
    def test_creation(self):
        personnages = Personnage.query.all()
        nb_personnages = len(personnages)
        entite = Entite()
        entite.save()
        personnage = Personnage(sexe=1, portrait='', entite_id=entite.id)
        personnage.save()
        personnages = Personnage.query.all()
        self.assertEqual(len(personnages), nb_personnages+1)

