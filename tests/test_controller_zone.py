#encoding: utf-8

from setup import Setup

class TestControllerZone(Setup):
    def test_acces_vue_hors_connexion(self):
        result = self.client.get('/zone/vue', follow_redirects=True)
        self.assertIn('Inscription', result.data.decode("utf-8"))

    def test_vue_ok(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo",
                "password": "foo",
            },
            follow_redirects=True
        )
        result = self.client.get('/zone/vue', follow_redirects=True)
        self.assertIn('Vue', result.data.decode("utf-8"))

    def test_vue_cible_sol(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo",
                "password": "foo",
            },
            follow_redirects=True
        )
        result = self.client.post(
            '/zone/vue',
            data = {
                "x": "2",
                "y": "2",
            },
            follow_redirects=True
        )
        self.assertIn('Sol', result.data.decode("utf-8"))

    def test_vue_cible_personnage(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo",
                "password": "foo",
            },
            follow_redirects=True
        )
        result = self.client.post(
            '/zone/vue',
            data = {
                "x": "1",
                "y": "1",
            },
            follow_redirects=True
        )
        self.assertIn('Héros foo', result.data.decode("utf-8"))

    def test_vue_cible_monstre(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo",
                "password": "foo",
            },
            follow_redirects=True
        )
        result = self.client.post(
            '/zone/vue',
            data = {
                "x": "2",
                "y": "1",
            },
            follow_redirects=True
        )
        self.assertIn('Lapin', result.data.decode("utf-8"))

    def test_generation_monstre(self):
        from app.model.entite import Entite
        nb_monstres = Entite.query.filter_by(zone_id=1, type=2).count()
        result = self.client.get('/zone/generate')
        self.assertEqual(nb_monstres+1, Entite.query.filter_by(zone_id=1, type=2).count())


if False:
    def test_creation_ok(self):
        result = self.client.post(
            '/personnage/creation',
            data = {
                "nom": "Héros foo2",
                "sexe": "1",
            },
            follow_redirects=True
        )
        self.assertIn('Vue', result.data.decode("utf-8"))

    def test_creation_personnage_deja_existant(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo2",
                "password": "foo2",
            },
            follow_redirects=True
        )
        result = self.client.post(
            '/personnage/creation',
            data = {
                "nom": "Héros foo",
                "sexe": "1",
            },
            follow_redirects=True
        )
        self.assertIn('Ce nom de personnage existe déjà', result.data.decode("utf-8"))

    def test_affichage_profile(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo",
                "password": "foo",
            },
            follow_redirects=True
        )
        result = self.client.get('/personnage/profil')
        self.assertIn('Profil', result.data.decode("utf-8"))

    def test_affichage_profil_sans_personnage(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo2",
                "password": "foo2",
            },
            follow_redirects=True
        )
        result = self.client.get('/personnage/profil', follow_redirects=True)
        self.assertIn("Création de votre personnage", result.data.decode("utf-8"))

    def test_action_sans_personnage(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo2",
                "password": "foo2",
            },
            follow_redirects=True
        )
        result = self.client.post(
            '/personnage/action',
            data = {
                "action": "deplacement",
                "x": "1",
                "y": "1",
            },
            follow_redirects=True
        )
        self.assertIn("Création de votre personnage", result.data.decode("utf-8"))

    def test_action_deplacement(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo",
                "password": "foo",
            },
            follow_redirects=True
        )
        from app.model.entite import Entite 
        entite = Entite.query.filter_by(id=1).first()
        old_pa = entite.pa
        result = self.client.post(
            '/personnage/action',
            data = {
                "action": "deplacement",
                "x": "1",
                "y": "2",
            },
            follow_redirects=True
        )
        entite = Entite.query.filter_by(id=1).first()
        self.assertEqual(1, entite.pos_x)
        self.assertEqual(2, entite.pos_y)
        self.assertEqual(old_pa-2, entite.pa)
        self.assertIn("Vue", result.data.decode("utf-8"))

    def test_action_attaque(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo",
                "password": "foo",
            },
            follow_redirects=True
        )
        from app.model.entite import Entite 
        entite = Entite.query.filter_by(id=1).first()
        old_pa = entite.pa
        result = self.client.post(
            '/personnage/action',
            data = {
                "action": "attaque",
                "x": "2",
                "y": "1",
            },
            follow_redirects=True
        )
        entite = Entite.query.filter_by(zone_id=1, pos_x=2, pos_y=1).first()
        self.assertIsNone(entite)
        entite = Entite.query.filter_by(id=1).first()
        self.assertEqual(old_pa-4, entite.pa)
        self.assertIn("Vue", result.data.decode("utf-8"))


if False:
    def test_chargement_regles(self):
        result = self.client.get('/regles')
        self.assertIn('Règles', result.data.decode("utf-8"))

    def test_chargement_regles_uand_connecte(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo",
                "password": "foo",
            }, follow_redirects=True
        )
        self.assertIn('Vue', result.data.decode("utf-8"))
        result = self.client.get('/regles')
        self.assertIn('Règles', result.data.decode("utf-8"))

    def test_chargement_inscription(self):
        result = self.client.get('/inscription')
        self.assertIn('Inscription', result.data.decode("utf-8"))

    def test_inscription_ok(self):
        result = self.client.post(
            '/inscription',
            data = {
                "login": "bar",
                "password": "bar",
                "confirmation": "bar",
                "email": "",
            }, follow_redirects=True
        )
        self.assertIn("Création de votre personnage", result.data.decode("utf-8"))

    def test_inscription_deja_existant(self):
        result = self.client.post(
            '/inscription',
            data = {
                "login": "foo",
                "password": "bar",
                "confirmation": "bar",
                "email": "",
            }, follow_redirects=True
        )
        self.assertIn("utilisateur existe déjà", result.data.decode("utf-8"))

    def test_connexion_apres_inscription_sans_personnage(self):
        result = self.client.post(
            '/inscription',
            data = {
                "login": "bar",
                "password": "bar",
                "confirmation": "bar",
                "email": "",
            }, follow_redirects=True
        )
        result = self.client.get('/deconnexion')
        result = self.client.post(
            '/connexion',
            data = {
                "login": "bar",
                "password": "bar",
            }, follow_redirects=True
        )
        self.assertIn("Création de votre personnage", result.data.decode("utf-8"))

    def test_connexion_erreur(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "bar",
                "password": "pouet",
            }, follow_redirects=True
        )
        self.assertIn("Accueil", result.data.decode("utf-8"))


