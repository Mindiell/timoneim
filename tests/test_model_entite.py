#encoding: utf-8

from setup import Setup

from app.model.entite import Entite

class TestModelEntite(Setup):
    def test_creation(self):
        entites = Entite.query.all()
        nb_entites = len(entites)
        entite = Entite()
        entite.save()
        entites = Entite.query.all()
        self.assertEqual(len(entites), nb_entites+1)

