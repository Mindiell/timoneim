#encoding: utf-8

from setup import Setup

class TestControllerCore(Setup):
    def test_chargement_accueil(self):
        result = self.client.get('/')
        self.assertIn('Accueil', result.data.decode("utf-8"))

    def test_chargement_regles(self):
        result = self.client.get('/regles')
        self.assertIn('Règles', result.data.decode("utf-8"))

    def test_chargement_regles_uand_connecte(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "foo",
                "password": "foo",
            },
            follow_redirects=True
        )
        self.assertIn('Vue', result.data.decode("utf-8"))
        result = self.client.get('/regles')
        self.assertIn('Règles', result.data.decode("utf-8"))

    def test_chargement_inscription(self):
        result = self.client.get('/inscription')
        self.assertIn('Inscription', result.data.decode("utf-8"))

    def test_inscription_ok(self):
        result = self.client.post(
            '/inscription',
            data = {
                "login": "bar",
                "password": "bar",
                "confirmation": "bar",
                "email": "",
            },
            follow_redirects=True
        )
        self.assertIn("Création de votre personnage", result.data.decode("utf-8"))

    def test_inscription_deja_existant(self):
        result = self.client.post(
            '/inscription',
            data = {
                "login": "foo",
                "password": "bar",
                "confirmation": "bar",
                "email": "",
            },
            follow_redirects=True
        )
        self.assertIn("utilisateur existe déjà", result.data.decode("utf-8"))

    def test_connexion_apres_inscription_sans_personnage(self):
        result = self.client.post(
            '/inscription',
            data = {
                "login": "bar",
                "password": "bar",
                "confirmation": "bar",
                "email": "",
            },
            follow_redirects=True
        )
        result = self.client.get('/deconnexion')
        result = self.client.post(
            '/connexion',
            data = {
                "login": "bar",
                "password": "bar",
            },
            follow_redirects=True
        )
        self.assertIn("Création de votre personnage", result.data.decode("utf-8"))

    def test_connexion_erreur(self):
        result = self.client.post(
            '/connexion',
            data = {
                "login": "bar",
                "password": "pouet",
            },
            follow_redirects=True
        )
        self.assertIn("Accueil", result.data.decode("utf-8"))


