-- On nettoie le contenu des tables
DELETE FROM monstre_zone;
DELETE FROM personnage;
DELETE FROM monstre;
DELETE FROM entite;
DELETE FROM zone;
DELETE FROM user;
DELETE FROM monstre_gabarit;

-- Administrateur
INSERT INTO user VALUES (1, 'admin', 'test', '', 1, 1);
-- Compte inactif
INSERT INTO user VALUES (2, 'inactive', 'test', '', 0, 1);
-- Utilisateurs
INSERT INTO user VALUES (3, 'foo', 'foo', 'foo@bar.com', 1, 0);
INSERT INTO user VALUES (4, 'foo2', 'foo2', 'foo2@bar.com', 1, 0);

-- Ajout d'une zone simple pour les tests : un rectangle de 3x3
INSERT INTO zone VALUES (1, 'Zone de test', 'zone_test', 100);

-- Ajout d'un personnage et de son entité
INSERT INTO entite VALUES (1, 1, 'Héros foo', 1, 110, 110, 110, 110, 110, 110, 110, 110, 10, 10, 10, 12, NOW(), 1, 1, 1, 1);
INSERT INTO personnage VALUES (1, 3, 1, 1, '', 0, 0, 0, 0);

-- Ajout d'un gabarit de monstre
INSERT INTO monstre_gabarit VALUES (1, 'Lapin de test', 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 12, 1, 1);
-- Ajout de quoi le générer
INSERT INTO monstre_zone VALUES (1, 1, 1, 100, 100);

-- Ajout d'un monstre et de son entité
INSERT INTO entite VALUES (2, 2, 'Lapin de test', 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 12, NOW(), 1, 2, 1, 1);
INSERT INTO monstre VALUES (1, 2, 1);

