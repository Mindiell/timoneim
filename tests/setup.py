#encoding: utf-8

import unittest

import server

class Setup(unittest.TestCase):
    def setUp(self):
        # Initialize application for testing
        self.app = server.app
        self.login_manager = server.login_manager
        self._ctx = self.app.test_request_context()
        self._ctx.push()
        # Initialize Database
        from app import db
        self.db = db
        self.populate()
        self.client = self.app.test_client()

    def tearDown(self):
        # Remove Database
        self.db.session.remove()
        # Close session
        with self.client.session_transaction() as my_session:
            my_session.clear()
        # Removing context
        if self._ctx is not None:
            self._ctx.pop()

    def populate(self):
        # Read external file
        with open("tests/populate.sql") as f:
            for line in f.read().splitlines():
                if line!='' and not line.startswith('--'):
                    self.db.engine.execute(line)

